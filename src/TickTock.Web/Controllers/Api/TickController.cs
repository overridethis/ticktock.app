﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TickTock.Core;
using TickTock.Core.Models;

namespace TickTock.Web.Controllers.Api
{
  [Route("api/[controller]")]
  public class TicksController : Controller
  {
    private readonly ITickService _tickService;

    public TicksController(ITickService tickService)
    {
      _tickService = tickService;
    }
    
    [HttpGet]
    public async Task<IEnumerable<Tick>> Get()
    {
      var ticks = await _tickService.Query();
      return ticks;
    }
    
    [HttpPost]
    public void Post([FromBody]Tick tick)
    {
      _tickService.Tick(tick);
    }
  }
}
