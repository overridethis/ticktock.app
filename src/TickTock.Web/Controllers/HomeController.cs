﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TickTock.Core;

namespace TickTock.Web.Controllers
{
  public class HomeController : Controller
  {
    private readonly ITickService _tickService;

    public HomeController(ITickService tickService)
    {
      _tickService = tickService;
    }

    public async Task<IActionResult> Index()
    {
      var ticks = (await _tickService.Query())
        .ToList();

      return View(ticks);
    }

    public IActionResult About()
    {
      ViewData["Message"] = "Your application description page.";
      return View();
    }

    public IActionResult Contact()
    {
      ViewData["Message"] = "Your contact page.";
      return View();
    }

    public IActionResult Error()
    {
      return View();
    }
  }
}
