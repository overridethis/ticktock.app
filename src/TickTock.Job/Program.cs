﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TickTock.Core;
using TickTock.Core.Models;
using Microsoft.EntityFrameworkCore;
using TickTock.Core.Impl;

namespace TickTock.Job
{
  public class Program
  {
    public static void Main(string[] args)
    {
      var config = Getconfiguration();
      var container = new ServiceCollection();
      container.AddDbContext<TickDbContext>(opt => opt.UseSqlServer(config["Data:TickDb"]));
      container.AddScoped<ITickService, TickService>();

      var provider = container.BuildServiceProvider();
      
      var svc = provider.GetService<ITickService>();
      for (var i = 0; i < 5; i++)
      {
        Console.WriteLine($"[Tick:{i}] {DateTime.UtcNow}");

        var tick = new Tick
        {
          TickTime = DateTime.UtcNow,
          Hostname = Environment.MachineName,
          AppVersion = "0.0.0-*"
        };

        svc.Tick(tick).Wait();
      }
      
#if DEBUG
      Console.WriteLine("Press any key to continue.");
      Console.ReadKey();
#endif
    }

    private static IConfiguration Getconfiguration()
    {
      var builder = new ConfigurationBuilder();

      return builder
        .AddJsonFile("./appsettings.json")
        .AddEnvironmentVariables()
        .Build();
    }
  }
}
