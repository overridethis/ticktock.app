dotnet restore src\TickTock.Job\project.json
dotnet restore src\TickTock.Web\project.json

dotnet build src\TickTock.Job\project.json --configuration Release
dotnet build src\TickTock.Web\project.json --configuration Release