## build.
Invoke-Expression ".\build.ps1"

## clean up
rm .\.release\ -Recurse -Force -ErrorAction Ignore

## build api package
dotnet publish "${pwd}\src\TickTock.Web\project.json" --output "${pwd}\.release\web" --configuration Release --no-build 
dotnet publish "${pwd}\src\TickTock.Job\project.json" --output "${pwd}\.release\web\App_Data\jobs\triggered\Tick\job\" --configuration Release --no-build